-- LUALOCALS < ---------------------------------------------------------
local include, nc
    = include, nc
-- LUALOCALS > ---------------------------------------------------------

nc.amcoremod()

include("quell")
include("api")
include("node")
include("lumps")
include("abm")
include("firestarting")
include("hints")
