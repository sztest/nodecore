-- LUALOCALS < ---------------------------------------------------------
local include, nc
    = include, nc
-- LUALOCALS > ---------------------------------------------------------

nc.amcoremod()

include("util")
include("api")
include("falling_ent")
include("item_ent")
include("item_merge")
include("fallthrufix")
