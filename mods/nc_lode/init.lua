-- LUALOCALS < ---------------------------------------------------------
local include, nc
    = include, nc
-- LUALOCALS > ---------------------------------------------------------

nc.amcoremod()

include("ore")
include("metallurgy")
include("anvils")
include("oresmelt")
include("tools")
include("shafts")
include("ladder")
include("adze")
include("tongs")
include("rake")
include("shelf")
include("hints")
