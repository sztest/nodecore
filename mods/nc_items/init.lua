-- LUALOCALS < ---------------------------------------------------------
local include, nc
    = include, nc
-- LUALOCALS > ---------------------------------------------------------

nc.amcoremod()

include('node')
include('bulknodes')
include('ent_item')
include('ent_falling')
include('hooks')
include('burnup')
include('pulverize')
include('throw_inertia')
include('drop_as')
include('hints')
include('protect')
