-- LUALOCALS < ---------------------------------------------------------
local nc
    = nc
-- LUALOCALS > ---------------------------------------------------------

nc.register_hint(
	"throw an item really fast",
	"item_drop_speed_14",
	"item_drop"
)
