-- LUALOCALS < ---------------------------------------------------------
local include, nc
    = include, nc
-- LUALOCALS > ---------------------------------------------------------

nc.amcoremod()

include("disable")
include("discover")
include("witness")
include("register")
include("state")
include("alerts")
include("reset")
