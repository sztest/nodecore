-- LUALOCALS < ---------------------------------------------------------
local include, nc
    = include, nc
-- LUALOCALS > ---------------------------------------------------------

nc.amcoremod()

include("amalgam")
include("pumice")
include("harden")
include("pumblob")
include("hints")
include("renew")
