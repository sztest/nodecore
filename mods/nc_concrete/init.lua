-- LUALOCALS < ---------------------------------------------------------
local include, nc
    = include, nc
-- LUALOCALS > ---------------------------------------------------------

nc.amcoremod()

include("api")
include("wet")
include("patterns")
include("node")
include("register")
include("stylus")
include("dungeon")
include("hints")
