-- LUALOCALS < ---------------------------------------------------------
local include, nc
    = include, nc
-- LUALOCALS > ---------------------------------------------------------

nc.amcoremod()

include("rushes")
include("sedges")
include("flowers")
include("thatch")
include("wicker")
include("cheat")
include("hints")
