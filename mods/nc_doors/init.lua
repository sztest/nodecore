-- LUALOCALS < ---------------------------------------------------------
local include, nc
    = include, nc
-- LUALOCALS > ---------------------------------------------------------

nc.amcoremod()

include("convey")
include("operate")
include("ablation")
include("register")
include("craft_catapult")
include("craft_press")
include("hints")
