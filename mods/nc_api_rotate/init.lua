-- LUALOCALS < ---------------------------------------------------------
local include, nc
    = include, nc
-- LUALOCALS > ---------------------------------------------------------

nc.amcoremod()

include("autofix")
include("api")
include("dots")
include("hud")
