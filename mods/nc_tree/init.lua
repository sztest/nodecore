-- LUALOCALS < ---------------------------------------------------------
local include, nc
    = include, nc
-- LUALOCALS > ---------------------------------------------------------

nc.amcoremod()

include("api")
include("node")
include("leafdecay")
include("compost")

include("stick")

include("schematic")
include("decor")

include("grow_node")
include("grow_active")

include("ambiance")

include("hints")
