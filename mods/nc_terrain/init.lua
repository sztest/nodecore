-- LUALOCALS < ---------------------------------------------------------
local include, nc
    = include, nc
-- LUALOCALS > ---------------------------------------------------------

nc.amcoremod()

include('api')
include('node')
include('dungeon')
include('biome')
include('strata')
include('ore')
include('abm')
include('ambiance')
include('hints')
