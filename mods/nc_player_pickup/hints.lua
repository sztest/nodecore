-- LUALOCALS < ---------------------------------------------------------
local nc
    = nc
-- LUALOCALS > ---------------------------------------------------------

nc.register_hint(
	"drop an item",
	"item_drop"
)

nc.register_hint(
	"drop all your items at once",
	"aux_item_drop",
	"item_drop"
)
