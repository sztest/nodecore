-- LUALOCALS < ---------------------------------------------------------
local include, nc
    = include, nc
-- LUALOCALS > ---------------------------------------------------------

nc.amcoremod()

include("setup")
include("breath")
include("hotbar")
include("touchtip")
include("looktip")
include("wieldtip")
include("crosshair")
include("pretrans")
include("cheats")
include("hints")
