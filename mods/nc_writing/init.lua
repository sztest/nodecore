-- LUALOCALS < ---------------------------------------------------------
local include, nc
    = include, nc
-- LUALOCALS > ---------------------------------------------------------

nc.amcoremod()

include("api")
include("node")
include("craft")
include("raking")
include("leaching")
include("hints")
