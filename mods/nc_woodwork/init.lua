-- LUALOCALS < ---------------------------------------------------------
local include, nc
    = include, nc
-- LUALOCALS > ---------------------------------------------------------

nc.amcoremod()

include("adze")
include("plank")
include("staff")
include("tools")
include("ladder")
include("shelf")
include("rake")
include("hints")
