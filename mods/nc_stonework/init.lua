-- LUALOCALS < ---------------------------------------------------------
local include, nc
    = include, nc
-- LUALOCALS > ---------------------------------------------------------

nc.amcoremod()

include("adze")
include("chip")
include("tools")
include("bricks")
include("dungeon")
include("hints")
