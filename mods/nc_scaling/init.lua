-- LUALOCALS < ---------------------------------------------------------
local include, nc
    = include, nc
-- LUALOCALS > ---------------------------------------------------------

nc.amcoremod()

include("api")
include("node")
include("abm")
include("placement")
include("tunnel")
include("displace")
include("hints")
