-- LUALOCALS < ---------------------------------------------------------
local nc
    = nc
-- LUALOCALS > ---------------------------------------------------------

nc.register_hint("navigate by touch in darkness", "craft:scaling light")
nc.register_hint("scale a wall", "scaling dy=0")
nc.register_hint("scale an overhang", "scaling dy=1", "scaling dy=0")
