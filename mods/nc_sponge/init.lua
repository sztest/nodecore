-- LUALOCALS < ---------------------------------------------------------
local include, nc
    = include, nc
-- LUALOCALS > ---------------------------------------------------------

nc.amcoremod()

include("node")
include("abm")
include("gen")
include("cultivate")
include("squeeze")
include("diving")
include("hints")
