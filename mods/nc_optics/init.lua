-- LUALOCALS < ---------------------------------------------------------
local include, nc
    = include, nc
-- LUALOCALS > ---------------------------------------------------------

nc.amcoremod()

include("api")
include("glass")
include("glued")
include("lens")
include("prism")
include("cooking")
include("crafting")
include("domainwalls")
include("shelf")
include("hints")
